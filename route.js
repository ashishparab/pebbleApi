var express = require('express');
var router = express.Router();

var path = require('path')

/* GET home page. */
router.use(function(req, res, next) {

  let url = req.url

  if(path.extname(url)){
      filePath = path.join(__dirname, 'public', 'data', url)
  }
  else if(url == '/brands-database/database/'){
      filePath =  path.join(__dirname, 'public', 'data', url, 'download')
  }
  else{
      filePath = path.join(__dirname, 'public', 'data', url, 'data.json')
  }

  res.sendFile(path.join(filePath))

});

module.exports = router;
